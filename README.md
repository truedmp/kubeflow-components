# kubeflow-components


## Index

- / [top2vec](./components/top2vec)
- / [doc2vec](./components/doc2vec)
- / [word2vec](./components/word2vec)
- / [great_expectations](./components/great_expectations)
- / [pandas_profiling](./components/pandas_profiling)
- / [recommendation_ai](./components/recommendation_ai) / [catalog - import](./components/recommendation_ai/catalog/import)
- / [recommendation_ai](./components/recommendation_ai) / [catalog - transform](./components/recommendation_ai/catalog/transform)
- / [recommendation_ai](./components/recommendation_ai) / [user_event - import](./components/recommendation_ai/user_event/import)
- / [recommendation_ai](./components/recommendation_ai) / [user_event - transform](./components/recommendation_ai/user_event/transform)
- / [slack](./components/slack)
- / [utilities](./components/utilities) / [exit_op](./components/utilities/exit_op)

## Current version

0.2.9

## Release detail

0.2.9

- Update `pandas profiling` dependency

0.2.8

- Add `is_only_fail` to `slack` component

0.2.7

- Add `top2vec` component

0.2.6 

- Add `doc2vec` component

0.2.5

- Support `create_time` field when transform product catalog for recommendation ai

0.2.4

- Bug fixed when generate pandas profiling

0.2.3

- Bug fixed when import user events for Google Recommendation AI

0.2.2

- Add `recommendation_ai/user_event/import` component
- Add `recommendation_ai/user_event/transform` component.

0.2.1

- Fix slack component by removing `{{workflow.status}}`

0.2.0

- Slack component support Argo workflow status.

0.1.0

- Draft components for the following components
    - `great_expectations`
    - `pandas_profiling`
    - `recommendation_ai/catalog/import`
    - `recommendation_ai/catalog/transform`
    - `slack`
    - `utilities/exit_op`