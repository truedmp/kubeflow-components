# Name

Train Doc2Vec model

# Labels

Doc2Vec, Google Cloud Storage, GCP, NLP

# Summary

A Kubeflow Pipeline component to prepare, train and export Doc2Vec model.

# Details

## Runtime arguments

| Argument    | Description | Optional | Data type | Accepted values | Default |
|-------------|-------------|----------|-----------|-----------------|---------|
| input_path  | Input path  | No | GCSPath |  gs://bucket/path/to/data.csv| |
| output_path | Output path for trained model | No | GCSPath | gs://bucket/path/to/trained_model | |
| key_column | Column which will be used as a key | Yes | String | id | id |
| texts_column | Column which will be used as a texts, it should be a list of words | Yes | String | texts | texts |
| doc2vec_args | Doc2Vec arguments. Please refer to [gensim's documentation](https://radimrehurek.com/gensim/models/doc2vec.html) | Yes | String | '{"vector_size": 10, "epochs": 10"}'` | `'{"vector_size": 10, "epochs": 10"}'`|

## Example of input file

The input file should contains at least 2 columns (`id` and `texts`)

| id | texts |
|----|-------|
| item_id_1 | ["lorem", "epsum"] |
| item_id_2 | ["the", "fox", "brown"] |


### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/doc2vec/component.yml"
doc2vec_op = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline

@pipeline(name='my-pipeline')
def my_pipeline(
    input_path="gs://path/to/input.csv",
    output_path="gs://path/to/trained_model.doc2vec",
    key_column="id",
    texts_column="texts",
    doc2vec_args=json.dumps({
        "vector_size": 10,
        "epochs": 10
    })
):
    doc2vec = doc2vec_op(
        input_path=input_path,
        output_path=output_path,
        key_column=key_column,
        texts_column=texts_column,
        doc2vec_args=doc2vec_args,
    )
```

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'

import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```