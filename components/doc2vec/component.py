from kfp.components import create_component_from_func
from kfp.dsl.types import GCSPath


def doc2vec_model(
    input_path: GCSPath(),
    output_path: GCSPath(),
    key_column: str="id",
    texts_column: str="texts",
    doc2vec_args: str='{"vector_size": 10, "epochs": 1}',
):
    """Reading data from Google Cloud Storage,
    Prepare and train Doc2vec model
    Export trained model to Google Cloud Storage

    Args:
        input_path: GCS Path to csv file for validation
        output_path: GCS Path to save trained model
        key_column: Column which will be used as a key
        texts_column: Column which will be used as a texts, it should be a list of words
        doc2vec_args: Doc2Vec arguments. Please refer to gensim documentation
            https://radimrehurek.com/gensim/models/doc2vec.html

    """
    import io
    import sys
    import json
    import logging
    import pandas as pd
    from ast import literal_eval
    from google.cloud import storage
    from gensim.models.doc2vec import Doc2Vec
    from gensim.models.doc2vec import TaggedDocument

    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt="%H:%M:%S",
        stream=sys.stdout
    )
    logger = logging.getLogger()

    def _read_from_gcs(gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()
        return io.BytesIO(result)

    def _upload_to_gcs(filename, gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(file_path)
        blob.upload_from_filename(filename)

    def _prepare_gensim_data(df, key_column='id', texts_column='texts'):
        for _, row in df.iterrows():
            yield TaggedDocument(row[texts_column], [row[key_column]])

    logger.info(f"Reading... {input_path}")
    data = _read_from_gcs(input_path)
    df = pd.read_csv(
        data,
        usecols=[key_column, texts_column],
        low_memory=True,
        converters={texts_column: literal_eval})
    df = df[~df[texts_column].isna()]
    logger.debug(df.head())
    logger.info("Preparing training data...")
    training_corpus = list(_prepare_gensim_data(df, key_column, texts_column))

    logger.info("Initialize model...")
    doc2vec_args = json.loads(doc2vec_args)
    model = Doc2Vec(**doc2vec_args)
    logger.info(f"Model Epoch: {model.epochs}")

    logger.info("Building vocab...")
    model.build_vocab(training_corpus)

    logger.info("Training vocab...")
    model.train(training_corpus, total_examples=model.corpus_count, epochs=model.epochs)
    logger.info("Training completed.")

    logger.info(f"Exporting model to {output_path}")
    model.save("./model")
    _upload_to_gcs("./model", output_path)


if __name__ == "__main__":
    create_component_from_func(doc2vec_model,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'google-cloud-storage==1.31.2',
                                   'gensim==4.2.0',
                                   'pandas==1.2.0'
                               ])
