from kfp.components import OutputPath, create_component_from_func
from kfp.dsl.types import GCSPath


def pandas_profiling_components(
        input_path: GCSPath(),
        sampling_ratio: float,
        mlpipeline_ui_metadata_path: OutputPath(),
):
    """Profiling CSV file

    Retrieve a CSV file from Google Cloud Storage, then run the pandas profiling on it.

    Args:
        input_path: GCS Path to CSV file
        sampling_ratio: Proportion of data for profiling (e.g., 0.2 = 20%)
        mlpipeline_ui_metadata_path: Path to mlpipeline ui
    
    Returns:
        None

    Raises:
        ValueError: An error occured when GCS Path is not exists.
    
    """
    import random
    import json
    import pandas as pd
    from pandas_profiling import ProfileReport
    from google.cloud import storage
    import io
    from loguru import logger

    def _read_from_gcs(gcs_path: str):
        storage_client = storage.Client()

        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()
        return io.BytesIO(result)

    logger.debug(f"Reading... {input_path}")
    data = _read_from_gcs(input_path)
    df = pd.read_csv(data, index_col=0, skiprows=lambda i: i>0 and random.random() > sampling_ratio)

    logger.debug("Profiling...")
    try:
        profile = ProfileReport(df, title="Profiling Report", explorative=True)
        profile_html = profile.to_html()
    except ZeroDivisionError as e:
        logger.error(f"ZeroDivisionError: {e}")
        logger.error("Could not calculate correlation. Generating report without correlations.")
        profile = df.profile_report(title="Report without correlations", correlations=None)
        profile_html = profile.to_html()

    logger.debug("Exporting profile...")
    metadata = {
        'outputs': [{
            'type': 'web-app',
            'storage': 'inline',
            'source': profile_html,
        }]
    }
    with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
        json.dump(metadata, metadata_file)
    return metadata


if __name__ == "__main__":
    create_component_from_func(pandas_profiling_components,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'pandas-profiling==3.3.0',
                                   'pandas==1.4.4',
                                   'google-cloud-storage==1.29.0',
                                   'loguru==0.5.3',
                                   'jinja2==3.0.3'
                               ])
