# Name

Make the component/pipeline failed

# Labels

Utility, Exit, Fail

# Summary

A Kubeflow Pipeline component which exit the component. Use to make the component failed.

# Details

### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/utilities/exit_op/component.yml"
exit_op = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline

@pipeline(name='my-pipeline')
def my_pipeline():
    exit_op()
```

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'
import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```