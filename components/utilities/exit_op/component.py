from kfp.components import create_component_from_func


def exit_op():
    import sys
    sys.exit(1)


if __name__ == "__main__":
    create_component_from_func(exit_op,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[])
