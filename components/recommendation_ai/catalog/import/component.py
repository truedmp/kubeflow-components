from kfp.components import create_component_from_func


def catalog_import(project_id: str, input_uris: str,
                   error_directory: str) -> str:
    """Import json line file to Google Recommendation AI

    Args:
        project_id: Google Cloud Platform Project ID
        input_uris: A comma-separated value of GCS Path (e.g. gs://path/to/file1,gs://path/to/file2)
        error_directory: A GCS Path to store error file from Recommendation AI

    Returns:
        Operation job name from Recommendation AI
    """
    from google.cloud.recommendationengine_v1beta1.services.catalog_service import CatalogServiceClient
    from google.cloud.recommendationengine_v1beta1.types import InputConfig, GcsSource
    from google.cloud.recommendationengine_v1beta1.types import ImportErrorsConfig
    from loguru import logger

    input_uris = [uri.strip() for uri in input_uris.split(",")]

    logger.debug(
        f"Importing... {input_uris} to Recommendation AI ({project_id})")
    client = CatalogServiceClient()
    operation = client.import_catalog_items(
        parent=
        f"projects/{project_id}/locations/global/catalogs/default_catalog",
        input_config=InputConfig(gcs_source=GcsSource(input_uris=input_uris)),
        errors_config=ImportErrorsConfig(gcs_prefix=error_directory))
    logger.debug(f"Import job name: {operation.operation.name}")

    return operation.operation.name


if __name__ == "__main__":
    create_component_from_func(catalog_import,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'google-cloud-recommendations-ai==0.2.2',
                                   "loguru==0.5.3"
                               ])
