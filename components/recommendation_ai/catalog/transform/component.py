from kfp.components import create_component_from_func

from kfp.dsl.types import GCSPath


def transform_to_rec_ai(input_gcs_path: GCSPath(),
                        output_gcs_path: GCSPath()) -> str:
    """Transform CSV file to JSON line, then save to GCS.

    Retrieve CSV file which contain column as followed.

    - id: String
    - title: String
    - description: String
    - categoryHierarchies: List of String
    - tags: List of String
    - languageCode: String
    - productMetadata__{nested_field_name}: According to Google Recommendation AI
        - e.g. productMetadata__images__uri: String
    - itemAttributes__{nested_field_name}: According to Google Recommendation AI
        - e.g. itemAttributes__categoricalFeatures__featureA: String/Int/Float
        - e.g. itemAttributes__numericalFeatures__featureB: String/Int/Float
    - create_time__seconds: Unix timestamp (example: 1598309835)

    Args:
        input_gcs_path: GCS Path to CSV file.
        output_gcs_path: GCS Path to save transformed JSON line file.

    Returns:
        Output GCS Path to trasnformed JSON line file.

    Raises:
        ValueError: An error occured when GCS Path is not exists.
    """
    import pandas as pd
    from ast import literal_eval
    import json
    import io
    from google.cloud import storage
    import datetime
    from loguru import logger

    SELECTED_COLUMNS = [
        'id', 'title', 'description', 'categoryHierarchies', 'productMetadata',
        'tags', 'languageCode', 'itemAttributes', 'create_time'
    ]

    def _get_gcs_bucket(gcs_path):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        bucket = storage_client.get_bucket(bucket_name)
        return bucket

    def _save_to_gcs(source_filename, gcs_path, filename):
        bucket = _get_gcs_bucket(gcs_path)
        file_path = '/'.join(gcs_path.split("/")[3:])

        blob = bucket.blob(f'{file_path}/{filename}')
        with open(source_filename, "rb") as file:
            blob.upload_from_file(file)

    def _read_from_gcs(gcs_path: str):
        bucket = _get_gcs_bucket(gcs_path)
        file_path = '/'.join(gcs_path.split("/")[3:])
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()
        return io.BytesIO(result)

    def _to_list(row):
        if isinstance(row, list):
            return row
        try:
            if row:
                return literal_eval(row)
            else:
                return []
        except:
            return []

    def _to_hierarchies(categories):
        _categories = []
        if isinstance(categories[0], dict):
            return categories
        for category in categories:
            _categories.append({"categories": [category]})
        return _categories

    def _recursive_value_assignment(name_parts, value):
        if len(name_parts) == 1:
            if value == value:
                return {name_parts[0]: value}
            return {}

        result = _recursive_value_assignment(name_parts[1:], value)
        if not result:
            return {}
        return {name_parts[0]: result}

    def _to_value(value):
        try:
            _value = literal_eval(value)
        except Exception as error:
            _value = value

        if isinstance(_value, list):
            return {"value": _value}

        return {"value": [_value]}

    def _double_underscore_to_dict(row):
        for name in row.keys():
            name_parts = name.split("__")
            if len(name_parts) > 1:
                if name_parts[0] == "itemAttributes":
                    if 'itemAttributes' not in row.keys():
                        row[name_parts[0]] = {
                            "categoricalFeatures": {},
                            "numericalFeatures": {}
                        }

                    if name_parts[1] == "categoricalFeatures":
                        row[name_parts[0]]['categoricalFeatures'].update(
                            _recursive_value_assignment(
                                name_parts[2:], row[name]))
                    if name_parts[1] == "numericalFeatures":
                        row[name_parts[0]]['numericalFeatures'].update(
                            _recursive_value_assignment(
                                name_parts[2:], row[name]))
                else:
                    if name_parts[0] not in row.keys():
                        row[name_parts[0]] = {}
                    row[name_parts[0]].update(
                        _recursive_value_assignment(name_parts[1:], row[name]))
        return row

    logger.debug(f"Reading... {input_gcs_path}")
    df = pd.read_csv(_read_from_gcs(input_gcs_path))

    # filter NA
    logger.debug(f"Filter... rows where categoryHierarchies is null")
    df = df[~df.categoryHierarchies.isna()].reset_index(drop=True)

    logger.debug("Transforming... to Recommendation AI")
    df.categoryHierarchies = df.categoryHierarchies.map(_to_list).map(
        _to_hierarchies)
    df.tags = df.tags.map(_to_list)

    for column in df.columns:
        if column.startswith("itemAttributes"):
            logger.debug(f"Transforming column: {column}")
            df[column] = df[column].map(_to_value)

    _df = df.apply(_double_underscore_to_dict, axis=1)

    filename = output_gcs_path.split("/")[-1]
    path = '/'.join(output_gcs_path.split("/")[:-1])

    logger.debug(f"Exporting dataframe to {filename}")
    with open(filename, 'w') as f:
        for line in _df[SELECTED_COLUMNS].to_dict(orient='records'):
            f.write(json.dumps(line))
            f.write("\n")

    logger.debug(f"Uploading to  dataframe to {path}/{filename}")
    current_date = datetime.datetime.now().strftime("%Y%m%d")
    _save_to_gcs(filename, f"{path}/{current_date}", filename)
    _save_to_gcs(filename, f"{path}", filename)

    return output_gcs_path


if __name__ == "__main__":
    create_component_from_func(transform_to_rec_ai,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'pandas==1.2.4',
                                   'google-cloud-storage==1.29.0',
                                   "loguru==0.5.3"
                               ])
