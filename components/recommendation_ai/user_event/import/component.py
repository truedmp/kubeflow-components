from kfp.components import create_component_from_func

def user_events_import(project_id: str, input_uris: str, errors_directory: str) -> str:
    """Import user events json line file to Google Recommendation AI

    Args:
        project_id: Google Cloud Platform Project ID
        input_uri: A GCS Path (e.g. gs://path/to/file1)
        error_directory: A GCS Path to store error file from Recommendation AI

    Returns:
        Operation job name from Recommendation AI
    """
    from google.cloud.recommendationengine_v1beta1.services.user_event_service import UserEventServiceClient
    from google.cloud.recommendationengine_v1beta1.types import InputConfig, ImportErrorsConfig, GcsSource
    from loguru import logger

    
    input_uris = [uri.strip() for uri in input_uris.split(",")]

    logger.debug(
        f"Importing... {input_uris} to Recommendation AI ({project_id})")

    input_config = InputConfig(gcs_source=GcsSource(input_uris=input_uris))
    error_config = ImportErrorsConfig(gcs_prefix=errors_directory)
    client = UserEventServiceClient()
    
    operation = client.import_user_events(
        parent=f"projects/{project_id}/locations/global/catalogs/default_catalog/eventStores/default_event_store",
        input_config=input_config,
        errors_config=error_config
    )
    logger.debug(f"Import job name: {operation.operation.name}")

    return operation.operation.name


if __name__ == "__main__":
    create_component_from_func(user_events_import,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'google-cloud-recommendations-ai==0.2.2',
                                   "loguru==0.5.3"
                               ])
