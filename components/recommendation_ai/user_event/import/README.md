# Name

Import user events to Recommendation AI

# Labels

Recommendation AI, User Events

# Summary

A Kubeflow Pipeline component to import user events from JSON line file to Recommendation AI. Then return the long-running import operation's name from Recommendation AI.

# Details

## Runtime arguments


| Argument    | Description | Optional | Data type | Accepted values | Default |
|-------------|-------------|----------|-----------|-----------------|---------|
| project_id  | Google Cloud Platform Project ID  | No | String |  project_id_12345 | |
| input_uris  | A comma-separated GCS path to json line files  | No | String |  gs://bucket/path/to/data.json, gs://bucket/path/to/data2.json | |
| errors_directory  | GCS path to store error files from Recommendation AI | No | String |  gs://bucket/path/to/error | |



## Output

| Name | Description | Type |
|------|-------------|------|
| operation_name | Long-running import operation's name returned from Recommendation AI | String



### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/recommendation_ai/user_event/import/component.yml"
user_event_import = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline

@pipeline(name='my-pipeline')
def my_pipeline(
    project_id="my_project_id",
    input_uris="gs://bucket/path/to/data1.json,gs://bucket/path/to/data2.json",
    errors_directory="gs://bucket/path/to/error"
):
    user_event_import(
        project_id,
        input_uris=input_uris,
        errors_directory=errors_directory)
```

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'
import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```