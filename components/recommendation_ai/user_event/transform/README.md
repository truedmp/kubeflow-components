# Name

Transform User Event CSV file to Recommendation AI JSON line format

# Labels

Recommendation AI, User Event

# Summary

A Kubeflow Pipeline component to transform user event CSV file to Recommendation AI JSON line file format. Then save to the Google Cloud Storage

# Details

## Runtime arguments

| Argument    | Description | Optional | Data type | Accepted values | Default |
|-------------|-------------|----------|-----------|-----------------|---------|
| event_type  | Type of user event  | No | String | home_page_view or category_page_view or detail_page_view or purchase_complete | |
| input_gcs_path  | GCS Path to CSV file  | No | String |  gs://bucket/path/to/csv_file.csv | |
| output_gcs_path  | GCS Path for transformed JSON line file  | No | String |  gs://bucket/path/to/output.json | |


## Output

| Name | Description | Type |
|------|-------------|------|
| output_gcs_path | GCS Path for transformed JSON line file | String



### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/recommendation_ai/user_event/transform/component.yml"
transform_user_event_to_rec_ai_op = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline

@pipeline(name='my-pipeline')
def my_pipeline(
    input_gcs_path="gs://bucket/path/to/file.csv",
    output_gcs_path="gs://bucket/path/to/transformed_file.json"
):
    transform_user_event_to_rec_ai_op(
        input_gcs_path=input_gcs_path
        output_gcs_path=output_gcs_path)
```

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'
import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```