from kfp.components import create_component_from_func
from kfp.dsl.types import GCSPath, String


def transform_user_event_to_rec_ai_components(event_type: String(),
                                              input_gcs_path: GCSPath(),
                                              output_gcs_path: GCSPath()):
    """Transform User Event CSV file to JSON line, then save to GCS.

    Retrieve CSV file which contains columns as followed.

    1. If event type is `home_page_view`.
        - eventTime: string (A timestamp in RFC3339 UTC "Zulu" format, with nanosecond resolution and up to nine fractional digits.)
        - eventType: String
        - userInfo: String
    2. If event type is `category_page_view`.
        - eventTime: string (A timestamp in RFC3339 UTC "Zulu" format, with nanosecond resolution and up to nine fractional digits.)
        - eventType: String
        - userInfo: String
        - productEventDetail__{nested_field_name}: According to Google Recommendation AI
            - e.g. productEventDetail__pageCategories__featureA: List
    3. If event type is `detail_page_view`.
        - eventTime: string (A timestamp in RFC3339 UTC "Zulu" format, with nanosecond resolution and up to nine fractional digits.)
        - eventType: String
        - userInfo: String
        - productEventDetail__{nested_field_name}: According to Google Recommendation AI
            - e.g. productEventDetail__productDetails__featureA: List
    3. If event type is `purchase_complete`.
        - eventTime: string (A timestamp in RFC3339 UTC "Zulu" format, with nanosecond resolution and up to nine fractional digits.)
        - eventType: String
        - userInfo: String
        - productEventDetail__{nested_field_name}: According to Google Recommendation AI
            - e.g. productEventDetail__productDetails__featureA: List
            - e.g. productEventDetail__purchaseTransaction__featureB: String/Int/Float

    Args:
        event_name: Type of user event which is `home_page_view`, `category_page_view`, `detail_page_view` or `purchase_complete`
        input_gcs_path: GCS Path to CSV file.
        output_gcs_path: GCS Path to save transformed JSON line file.

    Returns:
        Output GCS Path to trasnformed JSON line file.

    Raises:
        ValueError: An error occured when GCS Path is not exists.
    """

    from ast import literal_eval
    from os import path
    import pandas as pd
    import datetime
    from loguru import logger

    def _to_list(row):
        if isinstance(row, list):
            return row
        try:
            if row:
                return literal_eval(row)
            else:
                return []
        except:
            return []

    def transform_format(df, event_type):
        if event_type == 'home_page_view':
            select_columns = ['eventTime', 'eventType', 'userInfo']

        elif event_type == 'category_page_view':
            select_columns = ['eventTime', 'eventType', 'userInfo', 'productEventDetail']
            df.productEventDetail__pageCategories__categories = df.productEventDetail__pageCategories__categories.apply(
                _to_list)

        elif event_type == 'detail_page_view':
            select_columns = ['eventTime', 'eventType', 'userInfo', 'productEventDetail']
            df.productEventDetail__productDetails__id = df.productEventDetail__productDetails__id.apply(_to_list)

        elif event_type == 'purchase_complete':
            select_columns = ['eventTime', 'eventType', 'userInfo', 'productEventDetail']
            df.productEventDetail__productDetails__id = df.productEventDetail__productDetails__id.apply(_to_list)
            df.productEventDetail__productDetails__quantity = df.productEventDetail__productDetails__quantity.apply(
                _to_list)

        df.userInfo__visitorId = df.userInfo__visitorId.astype(str)
        df.userInfo__userId = df.userInfo__userId.astype('Int64').astype(str)
        df.userInfo__userId = df.userInfo__userId.replace({'<NA>': None})
        return df, select_columns

    def _recursive_value_assignment(name_parts, value):
        if len(name_parts) == 1:
            if value:
                result = {name_parts[0]: value}
            else:
                result = {}
        else:
            result = _recursive_value_assignment(name_parts[1:], value)
            if not result:
                result = {}
            else:
                result = {name_parts[0]: result}
        return result

    def _create_dict_in_list(key_part, value, key):
        if isinstance(value, list):
            new_list = [{key_part: v} if v else {} for v in value]
        else:
            raise Exception(f"Error, 'Value' of '{key}' should be list type")
        return new_list

    def _update_dict_in_list(old_dict_list, new_dict_list):
        result = []
        if old_dict_list:
            for old, new in list(zip(old_dict_list, new_dict_list)):
                old.update(new)
                if old.values():
                    result.append(old)
        else:
            result = new_dict_list

        return result

    def double_underscore_to_dict(row):
        for key, value in row.items():
            key_parts = key.split("__")

            if key_parts[0] not in row.keys():
                row[key_parts[0]] = {}
            if len(key_parts) > 1:
                # category page view
                if 'productEventDetail' in key_parts and 'pageCategories' in key_parts:
                    if 'pageCategories' not in row[key_parts[0]].keys():
                        row[key_parts[0]] = {
                            "pageCategories": [],
                        }
                    if key_parts[1] == "pageCategories":
                        new_list = _create_dict_in_list(key_parts[2], value, key)
                        row[key_parts[0]]['pageCategories'] = _update_dict_in_list(row[key_parts[0]]['pageCategories'],
                                                                                   new_list)

                # purchase complete
                elif 'productEventDetail' in key_parts and 'productDetails' in key_parts:
                    if 'productDetails' not in row[key_parts[0]].keys():
                        row[key_parts[0]].update({
                            "productDetails": [],
                        })
                    if key_parts[1] == "productDetails":
                        new_list = _create_dict_in_list(key_parts[2], value, key)
                        row[key_parts[0]]['productDetails'] = _update_dict_in_list(row[key_parts[0]]['productDetails'],
                                                                                   new_list)

                elif 'productEventDetail' in key_parts and 'purchaseTransaction' in key_parts:
                    if 'purchaseTransaction' not in row[key_parts[0]].keys():
                        row[key_parts[0]].update({
                            "purchaseTransaction": {},
                        })
                    if key_parts[1] == "purchaseTransaction":
                        row[key_parts[0]]['purchaseTransaction'].update(
                            _recursive_value_assignment(key_parts[2:], value))

                else:
                    row[key_parts[0]].update(_recursive_value_assignment(key_parts[1:], value))

        return row

    logger.debug(f"Reading... {input_gcs_path}")
    df = pd.read_csv(input_gcs_path)

    logger.debug("Transforming... to Recommendation AI")
    df, SELECTED_COLUMNS = transform_format(df, event_type)
    _df = df.apply(double_underscore_to_dict, axis=1)
    _df = _df[SELECTED_COLUMNS]

    output_file = output_gcs_path.split("/")[-1]
    main_directory_output_path = '/'.join(output_gcs_path.split("/")[:-1])
    current_date = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    default_output_gcs_path = path.join(main_directory_output_path, output_file)
    current_date_output_gcs_path = path.join(main_directory_output_path, current_date, output_file)

    logger.debug(f"Uploading to  dataframe to {default_output_gcs_path}")
    _df.to_json(default_output_gcs_path, orient='records', lines=True)
    _df.to_json(current_date_output_gcs_path, orient='records', lines=True)


if __name__ == "__main__":
    create_component_from_func(transform_user_event_to_rec_ai_components,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'pandas==1.3.0',
                                   'pyarrow==4.0.1',
                                   "loguru==0.5.3",
                                   'fsspec',
                                   'gcsfs'
                               ])