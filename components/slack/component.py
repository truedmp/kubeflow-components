from kfp.components import create_component_from_func

from typing import List


def notify_slack(
    webhook_url: str,
    title: str,
    pretext: str,
    fields: List,
    color: str = "#747d8c",
    workflow_status: str = None,
    is_only_fail: bool = False
) -> bool:
    """Send notification to Slack

    Args:
        webhook_url: A Slack webhook URL
        title: Title of the message
        pretext: Text to show as a message before an attachement
        fields: A list of title and values
        color: Color of the message bar
        workflow_status: Argo workflow status, If "Succeeded", color will be green, otherwise red.
        is_only_fail: True if you want to send the notification when the workflow status is failed, otherwise False (Default: False)
        Read more: https://api.slack.com/messaging/composing/layouts#attachments
    
    Returns:
        True when request success, otherwise False
    """

    import requests
    import json
    from loguru import logger

    _COLOR_GREEN = "#3ae374"
    _COLOR_RED = "#ff3838"
    _WORKFLOW_STATUS_SUCCESS = "Succeeded"

    if workflow_status:
        if workflow_status == _WORKFLOW_STATUS_SUCCESS:
            color = _COLOR_GREEN
        else:
            color = _COLOR_RED

    attachments = [{
        "fallback": title,
        "color": color,
        "pretext": pretext,
        "title": title,
        "fields": fields
    }]


    data = {"attachments": attachments}
    try:
        logger.debug(json.dumps(data))
        if not is_only_fail or (is_only_fail and workflow_status != _WORKFLOW_STATUS_SUCCESS):
            resp = requests.post(webhook_url, json=data)
            logger.debug(resp.status_code)
            logger.debug(resp.text)
    except Exception as error:
        logger.error(f"{type(error)}: {error}")
        return False
    return True


if __name__ == "__main__":
    create_component_from_func(
        notify_slack,
        output_component_file='component.yml',
        base_image='python:3.8',
        packages_to_install=['requests==2.25.1', "loguru==0.5.3"])
