# Name

Send the message to Slack channel

# Labels

Slack, Notification

# Summary

A Kubeflow Pipeline component send a message to Slack channel.

# Details

## Runtime arguments

| Argument    | Description | Optional | Data type | Accepted values | Default |
|-------------|-------------|----------|-----------|-----------------|---------|
| webhook_url  | A Slack webhook URL  | No | String | https://hooks.slack.com/services/AAAA/BBBB/CCCCCCC | |
| title  | Title of the message  | No | String |  Hello this is title | |
| pretext  | Text to show as a message before an attachement  | No | String |  This is pretext | |
| fields  | A list of title and values  | No | String | [{"title": "Title 1", "value": "Value 1"}] | |
| color  | Color of the message bar  | No | String | #4cd137 | #747d8c (Grey) |
| workflow_status |  Argo workflow status, e.g. (notify_slack(workflow_status="{{workflow.status}}")). If "Succeeded", color will be green, otherwise red. | No | String | "{{workflow.status}}" | |


## Output

| Name | Description | Type |
|------|-------------|------|
| Request's status | True when request success, otherwise False | Bool



### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/slack/component.yml"
notify_op = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline, ExitHandler

@pipeline(name='my-pipeline')
def my_pipeline(
    webhook_url="https://hooks.slack.com/services/AAAA/BBBB/CCCCCCC",
    title="This is title",
    color="#4cd137",
    pretext="This is pretext"
):
    # Using workflow status
    exit_task = notify_op(
        webhook_url=webhook_url,
        title=title,
        pretext=pretext,
        fields=[{"title": "Title 1", "value": "Value 1"}],
        workflow_status="{{workflow.status}}")

    with ExitHandler(exit_task):
        # Specify color
        notify_op(
            webhook_url=webhook_url,
            title=title,
            pretext=pretext,
            fields=[{"title": "Title 1", "value": "Value 1"}],
            color=color)
``` 

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'
import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```