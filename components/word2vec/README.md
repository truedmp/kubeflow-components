# Name

Train Word2Vec model

# Labels

Word2Vec, Google Cloud Storage, GCP, NLP

# Summary

A Kubeflow Pipeline component to prepare, train and export Word2Vec model.

# Details

## Runtime arguments

| Argument    | Description | Optional | Data type | Accepted values | Default |
|-------------|-------------|----------|-----------|-----------------|---------|
| input_path  | Input path  | No | GCSPath |  gs://bucket/path/to/data.csv| |
| output_path | Output path for trained model | No | GCSPath | gs://bucket/path/to/trained_model | |
| words_column | Column which will be used as a words, it should be a list of words | Yes | String | texts | texts |
| word2vec_args | Word2vec arguments. Please refer to [gensim's documentation](https://radimrehurek.com/gensim/models/word2vec.html) | Yes | String | `'{"vector_size": 10, "epochs": 10"}'` | `'{"vector_size": 10, "epochs": 10"}'` |
| input_file_format | Input File's Format | Yes | String | `CSV` or `NEWLINE_DELIMITED_JSON` | `CSV` |

## Example of input file

The input file should contains at least 1 column (`texts`)

| texts |
|-------|
| ["lorem", "epsum"] |
| ["the", "fox", "brown"] |


### How to use

#### 1. Load the component using KFP SDK

```python
from kfp.components import load_component_from_uri
component_uri = "https://bitbucket.org/truedmp/kubeflow-components/raw/master/components/word2vec/component.yml"
word2vec_op = load_component_from_url(component_uri)
```

#### 2. Set up the pipeline

```python
from kfp.dsl import pipeline

@pipeline(name='my-pipeline')
def my_pipeline(
    input_path="gs://path/to/input.csv",
    input_file_format="CSV",
    output_path="gs://path/to/trained_model.word2vec",
    words_column="words",
    word2vec_args=json.dumps({
        "vector_size": 10,
        "epochs": 10
    })
):
    word2vec = word2vec_op(
        input_path=input_path,
        output_path=output_path,
        words_column=words_column,
        word2vec_args=word2vec_args,
        input_file_format=input_file_format
    )
```

#### 3. Compile the pipeline


```python
pipeline_func = pipeline
pipeline_filename = pipeline_func.__name__ + '.zip'

import kfp.compiler as compiler
compiler.Compiler().compile(pipeline_func, pipeline_filename)
```

#### 4. Submit the pipeline for execution

```python
EXPERIMENT_NAME = "my_experiment"

import kfp
client = kfp.Client()
experiment = client.create_experiment(EXPERIMENT_NAME)

#Submit a pipeline run
run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)
```