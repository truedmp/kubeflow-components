from typing import Type
from kfp.components import create_component_from_func
from kfp.dsl.types import GCSPath


def word2vec_model(
        input_path: GCSPath(),
        output_path: GCSPath(),
        words_column: str="words",
        word2vec_args: str='{"vector_size": 10, "epochs": 1"}',
        input_file_format: str = "CSV",
):
    """Reading data from Google Cloud Storage,
    Prepare and train Word2Vec model
    Export trained model to Google Cloud Storage

    Args:
        input_path: GCS Path to csv file for validation
        output_path: GCS Path to save trained model, and projector.tensor files
        words_column: Column which will be used as a words, it should be a list of words
        word2vec_args: Word2Vec arguments. Please refer to gensim documentation
            https://radimrehurek.com/gensim/models/word2vec.html
        input_file_format: Input file format (Default: CSV). You can also use NEWLINE_DELIMITED_JSON

    """
    import io
    import sys
    import json
    import logging
    import pandas as pd
    from ast import literal_eval
    from google.cloud import storage
    from gensim.models.word2vec import Word2Vec

    CSV = "CSV"
    NEWLINE_DELIMITED_JSON = "NEWLINE_DELIMITED_JSON"

    logging.basicConfig(
        level=logging.DEBUG,
        format=
        "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt="%H:%M:%S",
        stream=sys.stdout)
    logger = logging.getLogger()

    def _read_from_gcs(gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()
        return io.BytesIO(result)

    def _upload_to_gcs(filename, gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(file_path)
        blob.upload_from_filename(filename)

    def _prepare_gensim_data(df, words_column='words'):
        for _, row in df.iterrows():
            yield row[words_column]

    logger.info(f"Reading... {input_path}")
    data = _read_from_gcs(input_path)
    
    if input_file_format.upper() == CSV:
        df = pd.read_csv(data, usecols=[words_column], low_memory=True, converters={words_column: literal_eval})
    elif input_file_format.upper() == NEWLINE_DELIMITED_JSON:
        df = pd.read_json(data, lines=True)
    else:
        raise TypeError(f"input_file_format is not supported ({CSV}, {NEWLINE_DELIMITED_JSON}), found '{input_file_format.upper()}'")
    
    df = df[~df[words_column].isna()]
    logger.debug(df.head())
    logger.info("Preparing training data...")
    training_corpus = list(_prepare_gensim_data(df, words_column))

    logger.info("Initialize model...")
    word2vec_args = json.loads(word2vec_args)
    model = Word2Vec(**word2vec_args)
    logger.info(f"Model Epoch: {model.epochs}")

    logger.info("Building vocab...")
    model.build_vocab(training_corpus)

    logger.info("Training vocab...")
    model.train(training_corpus,
                total_examples=model.corpus_count,
                epochs=model.epochs)
    logger.info("Training completed.")

    logger.info(f"Exporting model to {output_path}")
    model.save("./model")
    _upload_to_gcs("./model", output_path)


if __name__ == "__main__":
    create_component_from_func(word2vec_model,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'google-cloud-storage==1.31.2',
                                   'gensim==4.2.0',
                                   'pandas==1.2.0'
                               ])
