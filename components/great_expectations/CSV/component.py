from kfp.components import OutputPath, create_component_from_func
from kfp.dsl.types import GCSPath, String
from typing import List, NamedTuple


def validate_csv_using_greatexpectations(
    input_path: GCSPath(),
    expectation_suite_path: GCSPath(),
    string_columns: String(),
    list_columns: String(),
    mlpipeline_ui_metadata_path: OutputPath(),
) -> NamedTuple("Output", [("mlpipeline_ui_metadata", 'UI_metadata'),
                           ("validation_status", bool),
                           ("validation_result", List)]):
    """Validate CSV file using Great expectation.

    Retrieve csv file from google cloud storage and expectation suite,
    validate the file and export result as a mlpipeline UI.

    Args:
        input_path: GCS Path to csv file for validation
        expectation_suite_path: GCS Path to expectation suite for validation
        mlpipeline_ui_metadata_path: Path to mlpipeline ui

    Returns:
        A tuple of multiple outputs, including mlpipeline_ui_metadata,
        validation_status, and validation_result

        mlpipeline_ui_metadata: To visualize in Kubeflow
        validation_status: True if validation success, otherwise False
        validation_result: A list of failed tests

        {
            "mlpipeline_ui_metadata": UI_metadata,
            "validation_status": False,
            "validation_result": [{
                "title": "COLUMN_NAME",
                "value": "expect_column_to_be_unique, Fail (5/10)
            }]
        }
    
    Raises:
        ValueError: An error occured when GCS Path is not exists.
    """
    import json
    import great_expectations as ge
    from great_expectations.render import DefaultJinjaPageView
    from great_expectations.render.renderer import ValidationResultsPageRenderer
    from google.cloud import storage
    import io
    from collections import namedtuple
    from ast import literal_eval
    from loguru import logger

    _JSON = "json"
    _BYTES = "bytes"

    def _create_converters(string_columns: List, list_columns: List):
        converters = {}
        for col in string_columns:
            converters.update({col: str})
        for col in list_columns:
            converters.update({col: literal_eval})
        return converters

    def _read_from_gcs(gcs_path: str, return_as=_JSON):
        storage_client = storage.Client()

        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()

        if return_as == _JSON:
            return json.loads(result)
        elif return_as == _BYTES:
            return io.BytesIO(result)
        return result

    def _read_validation_result(validation_results):
        results = []
        for result in validation_results:
            is_success = result.get("success", False)
            experimentation_config = result.get("expectation_config")

            column_name = experimentation_config.get("kwargs", {}).get(
                "column", "UNKNOWN")
            expectation_condition = experimentation_config.get(
                "expectation_type", "UNKNOWN")
            expectation_result = result.get("result", {})
            unexpected_count = expectation_result.get("unexpected_count", 0)
            element_count = expectation_result.get("element_count", 0)

            if not is_success:
                results.append({
                    "title":
                    column_name,
                    "value":
                    f"{expectation_condition}, Fail {unexpected_count}/{element_count}",
                })
        return results

    logger.debug(f"Reading... {expectation_suite_path}")
    expectation_suite = _read_from_gcs(expectation_suite_path, return_as=_JSON)

    logger.debug(f"Reading... {input_path}")
    data = _read_from_gcs(input_path, return_as=_BYTES)

    logger.debug("Creating converters...")
    string_columns = [col.strip() for col in string_columns.split(',') if col.strip()]
    list_columns = [col.strip() for col in list_columns.split(',') if col.strip()]
    converters = _create_converters(string_columns, list_columns)

    logger.debug("Load data to dataframe...")
    df = ge.read_csv(data, expectation_suite=expectation_suite, converters=converters)

    logger.debug("Validating data...")
    result = df.validate()

    logger.debug("Writing validation result...")
    validation_result = result.to_json_dict().get("results", [])
    validation_result = _read_validation_result(validation_result)
    document_model = ValidationResultsPageRenderer().render(result)

    metadata = {
        'outputs': [{
            'type': 'web-app',
            'storage': 'inline',
            'source': DefaultJinjaPageView().render(document_model),
        }]
    }
    with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
        json.dump(metadata, metadata_file)

    output = namedtuple(
        "Output",
        ["mlpipeline_ui_metadata", "validation_status", "validation_result"])

    return output(mlpipeline_ui_metadata=metadata,
                  validation_status=result.success,
                  validation_result=validation_result)


if __name__ == "__main__":
    create_component_from_func(validate_csv_using_greatexpectations,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'great-expectations==0.13.11',
                                   'google-cloud-storage==1.29.0',
                                   'loguru==0.5.3',
                                   'jinja2==3.0.3'
                               ])
