from kfp.components import create_component_from_func
from kfp.dsl.types import GCSPath


def topc2vec_model(
    input_path: GCSPath(),
    output_path: GCSPath(),
    key_column: str="id",
    texts_column: str="texts",
    top2vec_args: str='{"embedding_model"="doc2vec","speed"="learn"}',
):
    """Reading data from Google Cloud Storage,
    Prepare and train Top2Vec model
    Export trained model to Google Cloud Storage

    Args:
        input_path: GCS Path to csv file for validation
        output_path: GCS Path to save trained model
        key_column: Column which will be used as a key
        texts_column: Column which will be used as a texts, it should be a list of words
        top2vec_args: Top2Vec arguments. Please refer to gensim documentation
            https://top2vec.readthedocs.io/en/latest/api.html

    """
    import io
    import sys
    import json
    import logging
    import pandas as pd
    from ast import literal_eval
    from google.cloud import storage
    from top2vec import Top2Vec

    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt="%H:%M:%S",
        stream=sys.stdout
    )
    logger = logging.getLogger()

    def _read_from_gcs(gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(file_path)
        if not blob:
            raise ValueError(f"Path {gcs_path} do not exists.")

        result = blob.download_as_string()
        return io.BytesIO(result)

    def _upload_to_gcs(filename, gcs_path: str):
        storage_client = storage.Client()
        bucket_name = gcs_path.split("/")[2]
        file_path = '/'.join(gcs_path.split("/")[3:])
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(file_path)
        blob.upload_from_filename(filename)
    
    def join_text(texts):
        return " ".join(texts)
    
    def tokenize(texts):
        return texts.split(" ")


    logger.info(f"Reading... {input_path}")
    data = _read_from_gcs(input_path)
    df = pd.read_csv(
        data,
        usecols=[key_column, texts_column],
        low_memory=True,
        converters={texts_column: literal_eval})
    df = df[~df[texts_column].isna()]
    logger.debug(df.head())
    logger.info("Preparing training data...")

    df[texts_column] = df[texts_column].apply(lambda x: join_text(x))
    doc_ids = list(df[key_column].values)
    doc_texts = list(df[texts_column].values)

    logger.info("Initialize model...")
    top2vec_args = json.loads(top2vec_args)
    model = Top2Vec(doc_texts, document_ids=doc_ids, tokenizer=tokenize ,**top2vec_args)

    logger.info(f"Exporting model to {output_path}")
    model.save("./model")
    _upload_to_gcs("./model", output_path)


if __name__ == "__main__":
    create_component_from_func(topc2vec_model,
                               output_component_file='component.yml',
                               base_image='python:3.8',
                               packages_to_install=[
                                   'google-cloud-storage==1.31.2',
                                   'top2vec==1.0.27',
                                   'pandas==1.2.0'
                               ])
